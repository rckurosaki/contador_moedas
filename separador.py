#necessario numpy (pip install numpy)
#necessario OpenCV

import numpy as np
import cv2 as cv
import argparse
import imutils
from imutils import perspective
from imutils import contours

#para calculo da distancia
from scipy.spatial import distance as dist


#==================================================================================
#================================== FUNCOES =======================================
#==================================================================================


#Ideia e converter para niveis de cinza para
#encontrar fronteiras atravez de erosao
def detectaFronteiras(imagem):
	imagem_cinza = cv.cvtColor(imagem, cv.COLOR_BGR2GRAY) #converte para niveis de cinza
	imagem_cinza = cv.GaussianBlur(imagem_cinza, (5,5), 0) #aplica borramento

	#Encontra-se fronteiras atravez do "Canny Edge Detection"
	#https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_canny/py_canny.html
	resultado = cv.Canny(imagem_cinza, 50, 100)
	#dilata as fronteiras encontradas e aplica erosao
	resultado = cv.dilate(resultado, None, iterations=1)
	resultado = cv.erode(resultado, None, iterations=1)

	return resultado





#pega os pontos da figura para cortar uma sub-imagem
def getMinMax(lista):
	x1 = int(min(map(lambda x : x[0], lista)))
	x2 = int(max(map(lambda x : x[0], lista)))

	y1 = int(min(map(lambda y : y[1], lista)))
	y2 = int(max(map(lambda y : y[1], lista)))

	return x1, x2, y1, y2



#==================================================================================
#================================== MAIN ==========================================
#==================================================================================

#le a imagem atraves do argumento da chamada do programa
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--imagem", required=True, help="Usar -i='caminho da imagem'\nEx: -i=Diretorio/imagem.jpg")
args = vars(ap.parse_args())

imagem = cv.imread(args["imagem"])


imagem_tratada = detectaFronteiras(imagem)
#cv.imshow("fronteiras", imagem_tratada)

#Acha os contornos das bordas das sub-imagens e as ordena
contorno = cv.findContours(imagem_tratada.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
contorno = imutils.grab_contours(contorno)
(contorno, _) = contours.sort_contours(contorno)

#lista que guarda as imagens cortadas
lista_moedas = []


#pega as areas e recorta cada moeda da imagem
#e armazena na lista de imagens "lista_moedas"
for (i, j) in enumerate(contorno):
	if cv.contourArea(j) < 100:
		continue

	areaCortar = cv.minAreaRect(j)
	areaCortar = cv.cv.BoxPoints(areaCortar) if imutils.is_cv2() else cv.boxPoints(areaCortar)
	areaCortar = np.array(areaCortar, dtype="int")
	#print(areaCortar)


	#area_corte = ordenaPontos(areaCortar)
	#print(recorte)

	x1, x2, y1, y2 = getMinMax(areaCortar)
	cortada = imagem[y1:y2, x1:x2]

	#Armazena a imagem na lista de imagens
	lista_moedas.append(cortada)



#Mostra nro de moedas encontradas
print("Nro de moedas:", len(lista_moedas))


#percorre por todas as imagens de moedas cortadas
for i in lista_moedas:

	#==================================================
	#  AQUI envia "cortada" para a nuvem classificar
	#==================================================
	cv.imshow("Cortada", i)

	#a cada tecla apertada vai passando pelos objetos encontrados
	#para melhor vizualizacao
	cv.waitKey(0)

cv.destroyAllWindows()
